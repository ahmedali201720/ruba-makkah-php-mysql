$(document).ready(function() {

    initiateCountrySelector();

    $(".country-dropdown-toggler").click(function(e) {

        e.preventDefault();
        e.stopPropagation();
        $(".countries-list", this).slideToggle(200);

    });

    $(".country-dropdown-toggler .country-item").click(function(e) {

        e.preventDefault();
        e.stopPropagation();
        $(".countries-list").slideUp(200);
        handleCountrySelect($(this));

    });

    $("body").click(function() {
        closeAnyCustomDropDown();
    });

    $("#phoneValue").change(function() {
        let id = $(this).attr('data-target');
        let inp = $(id);
        let key = $("#phoneKey").val();
        if (inp.length)
            inp.val(key + $(this).val());
    });

    function closeAnyCustomDropDown() {
        $(".custom-dropdown").slideUp(200);
    }

    function initiateCountrySelector() {
        let selectedCountry = $(".country-dropdown-toggler .countries-list .selected");
        if (selectedCountry.length == 1) {
            handleCountrySelect(selectedCountry);
        }
    }

    function handleCountrySelect(country) {
        let parentToggler = country.parent().parent();
        let imgSrc = $("img", country).attr('src');
        let countryName = $(".country", country).text();
        let countryCode = $(".country", country).attr('data-code');
        $(".active-country .image", parentToggler).attr('src', imgSrc);
        $(".active-country .name", parentToggler).text(countryName);
        let targetInputID = $(".active-country", parentToggler).attr('data-target');
        let targetInput = $(targetInputID);
        if (targetInput.length)
            changeCountryCode(countryCode, targetInput);
    }

    function changeCountryCode(code, target) {
        target.val(code);
    }

    function changePhone() {

    }

});