$(document).ready(function () {

    /* Contact form validation */

    var contactFormValidation = {
        'name': false,
        'region': false,
        'phone': false,
    }

    $("#fullName").keyup(function (e) {

        var result = validateText($(this).val(), 1);
        if (result) {
            $(this).removeClass('border-danger');
            $(this).parent().find('.validation-failed').hide(400);
            contactFormValidation['name'] = true;
        }
        else {
            $(this).addClass('border-danger');
            $(this).parent().find('.validation-failed').show(400);
            contactFormValidation['name'] = false;
        }

    });

    $("#phoneValue").keyup(function (e) {

        var result = validateMobile($(this).val(), 11);
        if (result) {
            $(this).removeClass('border-danger');
            $(this).parent().find('.validation-failed').hide(400);
            contactFormValidation['phone'] = true;
        }
        else {
            $(this).addClass('border-danger');
            $(this).parent().find('.validation-failed').show(400);
            contactFormValidation['phone'] = false;
        }

    });

    $("#region").change(function () {

        if ($(this).val()) {
            $(this).removeClass('border-danger');
            $(this).parent().find('.validation-failed').hide(400);
            contactFormValidation['region'] = true;
        }
        else {
            $(this).addClass('border-danger');
            $(this).parent().find('.validation-failed').show(400);
            contactFormValidation['region'] = false;
        }

    });

    $("#contactForm").submit(function (e) {

        emptyFieldObj = validateEmptyFields($("#fullName").val(), $("#phoneValue").val(), $("#region").val());
        if (!emptyFieldObj['nameValid']) {
            $("#fullName").parent().find('.validation-failed').show(400);
            contactFormValidation['name'] = false;
            e.preventDefault();
        }

        if (!emptyFieldObj['mobileValid']) {
            $("#phoneValue").parent().find('.validation-failed').show(400);
            contactFormValidation['phone'] = false;
            e.preventDefault();
        }

        if (!emptyFieldObj['regionValid']) {
            $("#region").parent().find('.validation-failed').show(400);
            contactFormValidation['region'] = false;
            e.preventDefault();
        }

        for (var key in contactFormValidation) {

            console.log(key, contactFormValidation[key])
            if (!contactFormValidation[key]) {
                e.preventDefault();
                break;
            }

        }

        var form = new FormData();
        form.append('fullName',$("#fullName").val());
        form.append('phone',$("#phone").val());
        form.append('region',$("#region").val());
        console.log(form)
        $.ajax({
            type: "POST",
            url: "http://localhost:90/ruba-makkah-v2/saveData.php",
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            success:  function(data){
                window.location.href = "http://localhost:90/ruba-makkah-v2/ar/thanks.html";
            }
        });
        e.preventDefault();
    });

    function validateText(value, min) {
        let count = value.length;
        if (count >= min) {
            return true;
        }
        else {
            return false;
        }
    }

    function validateMobile(value, count) {
        let numberCount = value.length;
        if (count == numberCount) {
            return true;
        }
        else {
            return false;
        }
    }

    function validateEmptyFields(name, mobile, region) {

        return {
            nameValid: name ? true : false,
            mobileValid: mobile ? true : false,
            regionValid: region ? true : false
        }

    }

});